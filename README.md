# Kulturkalender

Repo zum konfigurieren von:
- https://kulturkalender.asta.uni-goettingen.de
- https://fsrvkalender.service.asta.uni-goettingen.de/


[[_TOC_]]

## Kalender Einbetten
(für andere Kalender mit ensprechender base URL)

Füge einfach den `iframe`-Block, zu der Ansicht die du willst, in deine Webseite ein.

### Ansicht: Normal:
```html
<iframe src="https://kulturkalender.asta.uni-goettingen.de" title="Kulturkalender"></iframe>
```

[![Kulturkalender Ansicht Normal](images/normal.png)](https://kulturkalender.asta.uni-goettingen.de)

<iframe src="https://kulturkalender.asta.uni-goettingen.de" title="Kulturkalender"></iframe>


---

### Ansicht: `timeGridDay`:
```html
<iframe src="https://kulturkalender.asta.uni-goettingen.de/timeGridDay/now" title="Kulturkalender"></iframe>
```

[![Kulturkalender Ansicht timeGridDay](images/timegridday.png)](https://kulturkalender.asta.uni-goettingen.de/timeGridDay/now)

<iframe src="https://kulturkalender.asta.uni-goettingen.de/timeGridDay/now" title="Kulturkalender"></iframe>

---

### Ansicht: `timeGridWeek`:
```html
<iframe src="https://kulturkalender.asta.uni-goettingen.de/timeGridWeek/now" title="Kulturkalender"></iframe>
```

[![Kulturkalender Ansicht timeGridWeek](images/timegridweek.png)](https://kulturkalender.asta.uni-goettingen.de/timeGridWeek/now)

<iframe src="https://kulturkalender.asta.uni-goettingen.de/timeGridWeek/now" title="Kulturkalender"></iframe>

---

### Ansicht: `dayGridMonth`:
```html
<iframe src="https://kulturkalender.asta.uni-goettingen.de/dayGridMonth/now" title="Kulturkalender"></iframe>
```

[![Kulturkalender Ansicht dayGridMonth](images/daygridmonth.png)](https://kulturkalender.asta.uni-goettingen.de/dayGridMonth/now)

<iframe src="https://kulturkalender.asta.uni-goettingen.de/dayGridMonth/now" title="Kulturkalender"></iframe>

---

### Ansicht: `listMonth`:
```html
<iframe src="https://kulturkalender.asta.uni-goettingen.de/listMonth/now" title="Kulturkalender"></iframe>
```

[![Kulturkalender Ansicht listMonth](images/listmonth.png)](https://kulturkalender.asta.uni-goettingen.de/listMonth/now)

<iframe src="https://kulturkalender.asta.uni-goettingen.de/listMonth/now" title="Kulturkalender"></iframe>

---

### Ansicht: `listYear`:
```html
<iframe src="https://kulturkalender.asta.uni-goettingen.de/listYear/now" title="Kulturkalender"></iframe>
```

[![Kulturkalender Ansicht listYear](images/listyear.png)](https://kulturkalender.asta.uni-goettingen.de/listYear/now)

<iframe src="https://kulturkalender.asta.uni-goettingen.de/listYear/now" title="Kulturkalender"></iframe>

---

## How-To: Neuen Kalender Hinzufügen

### Neuen Kalender erstellen

1. Gehe auf [https://cloud.asta.uni-goettingen.de/apps/calendar](https://cloud.asta.uni-goettingen.de/apps/calendar).
2. Klicke auf "+ Neuer Kalender" und  wähle entweder "Neuer Kalender" oder "Neuer Kalender mit Aufgabenliste".
3. Gib dem Kalender noch einen Namen und schon hast du einen neuen Kalender erstellt.

### Kalender veröffentlichen und URL kopieren

4. Nachdem du nun den Kalender erstellt hast, klicke auf das ![Share Icon](images/share.png)-Icon oder das ![Shared Icon](images/shared.png)-Icon.
5. Falls du nun schon ein ![Öffentlichen-Link-Kopieren-Icon](images/copy.png)-Icon bei "Link teilen" hast, kannst du da nun einfach drauf klicken um den Öffentlichen Link zu kopieren. Falls nicht...
6. Klicke rechts neben "Link teilen" auf das "+" um den Kalender zu veröffentlichen. Nun kannst du auf das ![Öffentlichen-Link-Kopieren-Icon](images/copy.png)-Icon klicken um den Öffentlichen Link zu kopieren.

### Kalender zum Kulturkalender hinzufügen

7. Nun kannst du einen neuen Eintrag in der [kalender.json](kalender.json) erstellen, um deinen Kalender zum Kulturkalender hinzuzufügen.
Die verschiedenen Kalender sind über verscheidene Branches gelöst:

| Branch | link|
|--------|-----|
|Main| https://kulturkalender.asta.uni-goettingen.de |
|FSRV| https://fsrvkalender.service.asta.uni-goettingen.de |



## FAQ

### Wie muss ein Eintrag in der [kalender.json](kalender.json) aussehen?

Jeder Kalender muss entweder einen `public-url` oder `id` String haben, welcher den Link oder die ID zu einem Öffentlichen Kalender auf dem Nextcloud des AStAs hat.

Jeder Kalender sollte einen `name` und `comment` String haben.  `name` und `comment` werde nur für interne Zwecke verwendet und dienen dafür, dass man später noch weiß welche URL zu welchem Kalender gehört.

Jeder Kalender sollte ein `enabled` Boolean haben, welches bestimmt ob der Kalender mit auf der Kulturseite sein soll, oder nicht. Falls ein Kalender `enabled` nicht gesetzt hat, wird standardmäßig der Wert `true` angenommen.

### Wie wird der Kulturkalender deployed?

Wenn in diesem Repo ein Push-Event ausgelöst wird, feuert GitLab einen Webhook an das [Caprover vom AStA](https://captain.service.asta.uni-goettingen.de) welches dann den Dockercontainer aktuallisiert.

Der Dockercontainer selber macht dann einfach nur 302 Redirect auf die eigentliche Kulturkalender-URL auf Nextcloud.



