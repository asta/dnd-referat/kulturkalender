#!/bin/bash

set -e

JSON="$(cat kalender.json)"

URLPREFIX="$(echo "${JSON}" | jq -j '."url-prefix"')"

RES=""

function isstringcalendarid() {
	if [ "x$1" = "x" ]; then
		false; return
	fi

	if [ "x$(echo "${1}" | grep '^[0-9a-zA-Z]*$')" = "x" ]; then
		false; return
	fi

	true; return
}

NUMENTRYS="$(echo "$JSON" | jq -j '.entrys | length')"

echo "=> Found $NUMENTRYS entrys."

for ((i = 0; i < $NUMENTRYS  ; i++)); do
	CUR="$(echo "$JSON" | jq ".entrys[${i}]")"
	CURNAME="$(echo "$CUR" | jq -j '.name')"
	CURENABLED="$(echo "$CUR" | jq -j '.enabled')"
	if [ "x$CURENABLED" = "xtrue" ] || [ "x$CURENABLED" = "xnull" ]; then
		echo "=> Parsing entry[${i}]: $CURNAME"
		CURURL="$(echo "$CUR" | jq '."public-url"?,.id?' | grep '^"' | tr -d '"' | head -n 1)"
		CURID="$(echo "$CURURL" | sed 's,^.*apps/calendar/p/,,g' | cut -d '/' -f 1)"

		if ! isstringcalendarid "$CURID" ; then
			echo "ERROR: Entry doesn't contain (valid) calendar id!"
			echo "Entry causing this error:"
			echo "$CUR" | jq '.'
			exit 1
		fi

		echo "   ID: ${CURID}"
	
		RES="${RES}-${CURID}"

	else
		echo "=> Ignoring disabled entry[${i}]: $CURNAME"
	fi
	# empty line for cleaner output
	echo ""
done

echo "=> Finalizing URL"

RES="$(echo "${RES}" | sed 's,^-,,')"
RES="${URLPREFIX}${RES}"

echo "=> Building nginx config"
cat nginx.conf.template | sed "s,((URL)),${RES},g" > nginx.conf

echo "=> Done!"
echo "The final URL is: ${RES}"

