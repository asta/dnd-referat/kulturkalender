FROM ubuntu:latest

COPY . /app
WORKDIR /app

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y nginx jq bash

RUN bash ./build.sh

RUN rm -vf /etc/nginx/sites-enabled/*
RUN ln -s /app/nginx.conf /etc/nginx/sites-enabled/kulturkalender.conf

EXPOSE 80

ENTRYPOINT /bin/bash -c "/usr/sbin/nginx -t -g 'daemon off; master_process on;' && /usr/sbin/nginx -g 'daemon off; master_process on;'"

